//
//  MapVC.swift
//  MapRiset
//
//  Created by Alifudin Aziz on 25/03/20.
//  Copyright © 2020 Alifudin Aziz. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // SET INITIAL LOCATION INDOMARET
        //INDOMARET -> lokasi: pondok benowo indah
        let initialLocation = CLLocation(latitude: -7.240576, longitude: 112.629092)
        //INDOMARET -> lokasi: sememi
        // let initialLocation = CLLocation(latitude: -7.248053, longitude: 112.641823)
        
        // SET INITIAL LOCATION ALFAMART
        //ALFAMART -> lokasi: klakahrejo
        // let initialLocation = CLLocation(latitude: -7.250761, longitude: 112.646868)
        
        let regionRadius: CLLocationDistance = 1000
        func centerMapOnLocation(location: CLLocation) {
            let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                      latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
          mapView.setRegion(coordinateRegion, animated: true)
        }
        
        centerMapOnLocation(location: initialLocation)
        
        mapView.delegate = self
        
        // show detail place on map
        // masukkan locationName sesuai nama lokasi yg ada di initialLocation
        let artwork = NearbyStore(title: "Indomaret 24 Jam",
          locationName: "Pondok Benowo Indah 2",
          discipline: "Store",
          coordinate: CLLocationCoordinate2D(latitude: -7.240576, longitude: 112.629092))
        mapView.addAnnotation(artwork)
    }
    
}

extension MapVC: MKMapViewDelegate {
  // 1
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    // 2
    guard let annotation = annotation as? NearbyStore else { return nil }
    // 3
    let identifier = "marker"
    var view: MKMarkerAnnotationView
    // 4
    if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
      as? MKMarkerAnnotationView {
      dequeuedView.annotation = annotation
      view = dequeuedView
    } else {
      // 5
      view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
      view.canShowCallout = true
      view.calloutOffset = CGPoint(x: -5, y: 5)
      view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    return view
  }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
        calloutAccessoryControlTapped control: UIControl) {
      let location = view.annotation as! NearbyStore
      let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
      location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}
