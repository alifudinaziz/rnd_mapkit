//
//  ViewController.swift
//  MapRiset
//
//  Created by Alifudin Aziz on 25/03/20.
//  Copyright © 2020 Alifudin Aziz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnMap(_ sender: UIButton) {
        performSegue(withIdentifier: "segueMap", sender: self)
    }
    
}

